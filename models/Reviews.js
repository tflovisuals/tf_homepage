import mongoose from 'mongoose'

const ReviewSchema = new mongoose.Schema({
  name: { type: String, required: true },
  event: { type: String, required: true },
  review: { type: String, required: true },
  date: {
    type: Date,
    default: Date.now(),
    required: true
  }
})

module.exports = mongoose.models.Review || mongoose.model('Review', ReviewSchema)
