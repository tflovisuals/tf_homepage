import mongoose from 'mongoose'

const InquireSchema = new mongoose.Schema({
    name: { type: String, required: 'Please enter a name.' },
    email: { type: String, trim: true, required: 'Please enter a email.', match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'Please enter a valid email.'],},
    phone: { type: String, match: [/^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/, 'Please enter a valid phone number.'],  required: 'Please enter a phone number.' },
    location: { type: String, required: 'Please enter the event location.' },
    eventDate: { type: Date, min: Date.now(), required: 'Please enter a date.' },
    needs: { type: [String], required: 'Please select event needs.'},
    categories: { type: [String], required: 'Please select event categories'},
    message: { type: String },
    inquireDate: { type: Date, default: Date.now(), required: true }
})

module.exports = mongoose.models.Inquire || mongoose.model('Inquire', InquireSchema)