import React from 'react'
import styled from 'styled-components'
import { motion } from 'framer-motion'

const NavButton = styled.div`
  cursor: pointer;
  justify-content: flex-end;
  z-index: 99;

  @media screen and (${props => props.theme.size.lg}){
    margin-top: 2em;
    margin-right: 4em;
    padding-bottom: 1em;
  }
  @media screen and (${props => props.theme.size.m}){
    margin-top: 2em;
    margin-right: 4em;
    padding-bottom: 1em;
  }
  @media screen and (${props => props.theme.size.sm}){
    margin-top: 2em;
    margin-right: 2em;
  }
  @media screen and (${props => props.theme.size.xs}){
    margin-top: 2em;
    margin-right: 2em;
    padding-bottom: 1em;
  } 
`

const Path = (props) => (
  <motion.path
    fill='transparent'
    stroke='white'
    strokeLinecap='round'
    strokeWidth='3'
    {...props}
  />
)

const transition = { ease: 'easeInOut', duration: 0.33 }

function NavToggle ({ toggle, isOpen }) {
  return (
    <NavButton onClick={toggle}>
      <svg width='23' height='23' viewBox='0 0 23 23'>
        <Path
          animate={isOpen ? 'open' : 'closed'}
          initial={false}
          variants={{
            closed: { d: 'M 2 2.5 L 20 2.5' },
            open: { d: 'M 3 16.5 L 17 2.5', stroke: '#6F6F71' }
          }}
          transition={transition}
        />
        <Path
          d='M 2 9.423 L 20 9.423'
          stroke='hsl(0, 0%, 18%)'
          animate={isOpen ? 'open' : 'closed'}
          initial={false}
          variants={{
            closed: { opacity: 1, stroke: 'white' },
            open: { opacity: 0 }
          }}
          transition={transition}
        />
        <Path
          animate={isOpen ? 'open' : 'closed'}
          initial={false}
          variants={{
            closed: { d: 'M 2 16.346 L 20 16.346' },
            open: { d: 'M 3 2.5 L 17 16.346', stroke: '#6F6F71' }
          }}
          transition={transition}
        />
      </svg>
    </NavButton>
  )
}

export default NavToggle
