import React from 'react'

// Components
import Title from '../common/Title'
import SectionNum from '../common/SectionNum'
import TestimonialCard from '../TestimonialCard'

import * as s from './TestimonialStyles'

const Testimonial = ({ reviews }) => {
  return (
    <s.Container id='four'>
      <s.TitleWrapper>
        <Title>Testimonials</Title>
      </s.TitleWrapper>
      <s.NumWrapper>
        <SectionNum>04</SectionNum>
      </s.NumWrapper>
      <s.ContentWrapper>
        <s.PrimaryPhotoPlace />
        <s.ReviewWrapper>
          {reviews.map((review, id) => (
            <TestimonialCard key={id} customerReview={review} />
          ))}
        </s.ReviewWrapper>
      </s.ContentWrapper>
    </s.Container>
  )
}

export default Testimonial
