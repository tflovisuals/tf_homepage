import styled from 'styled-components'
import { motion } from 'framer-motion'

export const Container = styled.div`
  background-color: ${props => props.theme.colors.primary};
  display: flex;
  flex-direction: column;
  margin: 5% 10%;

  @media screen and (${props => props.theme.size.m}) {
  }
`

export const ContentWrapper = styled.div`
  display: flex;
  flex-direction: row;

  @media screen and (${props => props.theme.size.m}) {
    flex-direction: column;
  }
`

export const TitleWrapper = styled.div`
  direction: rtl;
`

export const NumWrapper = styled.div`
direction: ltr;
position: absolute;
`

export const ReviewWrapper = styled.div`
  display: flex;
  flex-direction: column;
  overflow: scroll;
  scroll-behavior: smooth;
  flex:1;
  margin: 2rem 0;

  @media screen and (${props => props.theme.size.m}){
    flex:0;
  }
`

export const PrimaryPhotoPlace = styled(motion.div)`
  position: relative;
  direction: ltr;
  background-image: url(ATXReflection.jpg);
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 0px 350px 350px 0px;
  width: 888px;
  height: 520px;
  left: -12%;
  border-left: 5px solid ${props => props.theme.colors.secondary};

  @media screen and (${props => props.theme.size.m}) {
    width: 100%;
    height: 300px;
    display: none;
  }
`
