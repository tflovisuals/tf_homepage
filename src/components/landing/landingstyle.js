import styled from 'styled-components'
import { motion } from 'framer-motion'

export const BgSVGCore = styled.div`
  position:absolute;
  z-index: 1;
  right: 10%;
  top: 5%;
  animation-duration: 1.2s;
  animation-name: animateCore;
  animation-timing-function: ease-in;
  animation-direction: alternate;
  mix-blend-mode: soft-light;

  @keyframes animateCore {
    from{ 
      opacity: 0;

    }
    to {
      opacity: 1;
    }
  }

`
export const LandContainer = styled.div`
  background-image: url('DSC03635.png');
  background-repeat: no-repeat;
  background-size: cover;
  text-align: center;
  display: flex;
  flex-direction: column;
  width: 100%;

  @media screen and (${props => props.theme.size.m}){
    margin-bottom: 50px;
    background-size: 2800px 2900px;
    background-position: right;
    height: 100vh;
  }
`
export const TreeLine = styled.div`
  position: relative;
  display: block;

  @media screen and (${props => props.theme.size.m}) {
    top: 80%;
  }
`
export const ArrowBtn = styled.div`
  position: absolute;
  bottom: 20%;
  left: 10%;
`

export const Logo = styled.div`
  position: relative;
  margin: 0px auto;
  top: 150px;
  left: 0;
  width: 70%;
  align-content: center;
  justify-self: center;
  vertical-align: middle;

  @media screen and (${props => props.theme.size.m}){
    height: fit-content;
    width: 90%;
    top: 30%;
  }
`
export const BoldStatement = styled(motion.div)`
  font-size: 24px;
  font-weight: 700;
  color: ${props => props.theme.colors.textinvert};
`

export const Statement = styled(motion.div)`
  padding: 0.5em 1em;
  vertical-align: center;
  justify-content: center;
  color: ${props => props.theme.colors.textinvert};
  font-size: 22;
  font-weight: 200;

  @media screen and (${props => props.theme.size.sm}){
    right: 1em;
    bottom: 15%;

  }

`
export const SM = styled(motion.div)`
  vertical-align: center;
  justify-content: center;
  color: ${props => props.theme.colors.textinvert};
  font-size: 22px;
  font-weight: 200;
  font-style: italic;
  text-decoration: underline;

  :hover {
    color: ${props => props.theme.colors.secondary}
  }
`

export const CardNum = styled(motion.p)`
  font-size: 65px;
  position: absolute;
  left: 3%;
  top: 10%;
  font-weight: 700;
  color: ${props => props.theme.colors.secondary};
  filter: opacity(40%);
`
export const Name = styled(motion.div)`
  font-size: 38px;
  position: absolute;
  right: 5%;
  top: 15%;
  font-weight: 400;
  filter: opacity(80%);
  color: ${props => props.theme.colors.secondary};

  @media screen and (${props => props.theme.size.m}){
    display: none;
  }
`
