/**
 * @file
 * Landing file block
 */

import React from 'react'
import Image from 'next/image'
import * as s from './landingstyle'

const landing = () => {
  const smmotion = {
    visible: { opacity: 1, y: 0, transition: { type: 'spring', ease: [0.17, 0.67, 0.83, 0.67], bounce: 0.60, duration: 3.0 } },
    hidden: { opacity: 0, y: 100 }
  }

  const statemotion = {
    visible: { opacity: 1, y: 0, transition: { type: 'easein', ease: [0.17, 0.67, 0.83, 0.67], duration: 3.0 } },
    hidden: { opacity: 0, y: 10 }
  }

  return (
    <s.LandContainer id='one'>
      <s.CardNum>
        01
      </s.CardNum>
      <s.Name>
        Tyler J. Flores
      </s.Name>
      <s.Logo>
        <Image src='/tflo-visuaks_full_white.svg' alt='TFLO' loading='eager' priority width={1360} height={500} />
      </s.Logo>
      <s.TreeLine>
        <Image src='/TreeLine.png' alt='' priority layout='responsive' width='1680' height='320' />
      </s.TreeLine>
      <s.ArrowBtn>
        <Image src='/Arrowdown.svg' alt='' priority width='50' height='50' />
      </s.ArrowBtn>
    </s.LandContainer>
  )
}

export default landing
