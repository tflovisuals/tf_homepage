import styled from 'styled-components'
import { motion, animate } from 'framer-motion'

// Styled Components for About.js

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  background-color: ${props => props.theme.colors.primary};
  height: 100vh;
  margin: 4rem 0;

  @media screen and (${props => props.theme.size.m}) {
    flex-direction: column;
  }
`

export const AboutWrapper = styled.div`
  border-top: 1px solid ${props => props.theme.colors.secondary};
  display: flex;
  flex-direction: column;
  margin-left: 10%;
  margin-top: 5%;
`
export const AboutContent = styled.div`
  display: flex;
  flex-direction: row;
`
export const AboutWord = styled.div`
  display: flex;
  flex-direction: column;
`

export const ImageWrapper = styled.div`
  margin-top: 10%;
  margin-left: 15%;
`
export const ArrowWrapper = styled.div`
    position: absolute;
    margin-top: 150px;
    left: 5%;
    @media screen and (${props => props.theme.size.m}) {
      display: none;
    }
`

export const Title = styled.h1`
  color: ${props => props.theme.colors.textinvert};
`

export const Statement = styled(motion.p)`
  color: ${props => props.theme.colors.textinvert};
  font-size: 16px;
  line-height: 30px;
  letter-spacing: 1px;
  margin: 4rem 0;
  @media screen and (${props => props.theme.size.m}) {
    padding: 10px;
  }
`

export const RadialBlur = styled(motion.ellipse)`
  width: 100px;
  height: 100px;
  color: ${props => props.theme.colors.secondary};
`

export const SVGParent = styled(motion.svg)`
  position: absolute;
  width: 150px;
  height: 150px;
`

export const RadialShape = styled(motion.path)`
  height: 350px;
  width: 350px;
  border: 1px solid ${props => props.theme.colors.secondary};
  border-radius: 50%;
  display: inline-block;
  position: absolute;
  right: 45%;
  margin-top: -25rem;
`

export const DirectionShape = styled(motion.path)`
  height: 150px;
  width: 150px;
  border-radius: 50%;
  background: radial-gradient( ${props => props.theme.colors.secondary}, transparent);
  display: inline-block;
  position: absolute;
  right: 45%;
  margin-top: -10rem;
`

export const PrimaryPhotoPlace = styled(motion.div)`
  position: relative;
  direction: rtl;
  background-image: url(tflofootup.jpeg);
  background-repeat: no-repeat;
  background-size: cover;
  border-radius: 350px 0px 0px 350px;
  width: 888px;
  height: 520px;
  align-items: right;
  border-right: 5px solid ${props => props.theme.colors.secondary};

  @media screen and (${props => props.theme.size.m}) {
    width: 100%;
    height: 300px;
  }
`

export const HZBorder = styled(motion.div)`
  border-top: 1px solid ${props => props.theme.colors.secondary};

  @media screen and (${props => props.theme.size.m}) {
    display: none;
  }
`

// Framer-Motion constants for About.js components

export const IconVariants = {
  hidden: {
    opacity: 0,
    pathLength: 0
  },
  visible: {
    opacity: 1,
    pathLength: 1
  },
  transition: {
    default: { duration: 2, ease: 'easeInOut' },
    fill: { duration: 2, ease: [1, 0, 0.8, 1] }
  }
}

export const PhotoVariants = {
  from: {},
  to: {}
}

export const StatementVariants = {
  from: {},
  to: {}
}

export const TitleVariant = {
  from: {},
  to: {}
}

export const DirectionShapeVariants = {
  hidden: {
    opacity: 0,
    pathLength: 0,
    fill: 'rgba(255, 255, 255, 0)'
  },
  visible: {
    opacity: 1,
    pathLength: 1,
    fill: 'rgba(255, 255, 255, 1)'
  }
}

export const AboutStatement = 'T-Flo Visuals is an independent film/video creator located in Austin, TX. What started as a personal hobby has now developed into a passionate business. Our digital world allows us to express ourselves through videos & photos telling our stories while sharing our experiences with family & friends. Weather it’s capturing memories, promoting your business/services, or a personal film , T-Flo Visuals strives to visually express the story you envision. Let’s make flick 📸🎞📽'
