/**
 * @file
 * About file block.
 *
 */

import React from 'react'
import Image from 'next/image'
import SectionNum from 'src/components/common/SectionNum'
import * as s from './AboutStyle'

const About = () => {
  return (
    <s.Container id='two'>
      <s.AboutWrapper>
        <s.AboutContent>
          <SectionNum>
            02
          </SectionNum>
          <s.AboutWord>
            <s.Title>About</s.Title>
            <s.Statement>{s.AboutStatement}</s.Statement>
          </s.AboutWord>
        </s.AboutContent>
        <s.ArrowWrapper>
          <Image src='/VisualWorkDirection.svg' alt='' width='100' height='400' />
        </s.ArrowWrapper>
      </s.AboutWrapper>
      <s.ImageWrapper>
        <s.PrimaryPhotoPlace />
        <s.RadialShape />
        <s.DirectionShape />
      </s.ImageWrapper>
    </s.Container>
  )
}

export default About
