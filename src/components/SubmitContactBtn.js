import React from 'react'
import styled from 'styled-components'

const SubmitContactBtn = ({ submit, name, content }) => {
  return (
    <StyledButton name={name} onClick={() => submit()}>
      {content}
    </StyledButton>
  )
}

const StyledButton = styled.a`
  display: inline-block;
  border-radius: 3px;
  padding: 0.5rem 0;
  margin: 0.5rem 1rem;
  background: transparent;
  color: white;
  border: 2px solid white;
  text-align: center;


  :active, :focus, :focus-within, :hover {
    background-color: ${props => props.theme.colors.secondary};
    border: 3px solid white;
    cursor: pointer;
  }


  @media (min-width: 768px) {
    padding: 1rem 2rem;
    width: 11rem;
  }

  @media (min-width: 1024px) {
    padding: 1.5rem 2.5rem;
    width: 13rem;
  }

  @media screen and (${props => props.theme.size.m}) {
    width: 80%;
  }
`

export default SubmitContactBtn
