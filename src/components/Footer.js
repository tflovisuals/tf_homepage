import React from 'react'
import styled from 'styled-components'
import Image from 'next/image'
import { FaInstagram, FaTiktok } from 'react-icons/fa'

const Footing = styled.div`
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.colors.primary};
  display: flex;
  flex-direction: column;
  margin: 10px 0;
  padding: 10px;
`
const SocialContiner = styled.div`
  flex-direction: row;
  display: flex;
`
const LinkedLogo = styled.a`
  justify-content: center;
  align-items: center;
  margin: 10px 10px;
  border: 1px solid white;
  padding: 10px;
  border-radius: 100%;
  color: white;

  :hover{
    color: ${props => props.theme.colors.secondary};
    border-color: ${props => props.theme.colors.secondary};
  }
`

const Rights = styled.p`
  color: ${(props) => props.theme.colors.textinvert};
  text-align: center;
  font-size: 12px;
  margin: 20px 0;

  @media screen and (${props => props.theme.size.m}){
    font-size: 10px;
  }

  @media screen and (${props => props.theme.size.sm}){
    font-size: 10px;
  }

  @media screen and (${props => props.theme.size.xs}){
    font-size: 10px;
  }
`
const Footer = () => {
  return (
    <Footing>
      <SocialContiner>
        <LinkedLogo
          href="https://www.instagram.com/tflovisuals/"
          target='_blank'
          rel='Instsgram Link'
        >
          <FaInstagram/>
        </LinkedLogo>
        <LinkedLogo
          href="https://www.tiktok.com/@tflovisuals"
          target='_blank'
          rel='Tiktok Link'
        >
          <FaTiktok/>
        </LinkedLogo>
      </SocialContiner>
      <Rights>TFLO-Visuals. © 2020-2022 | ALL RIGHTS RESERVED</Rights>
   </Footing>
  )
}

export default Footer
