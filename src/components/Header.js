import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import DeskNav from 'src/components/DeskNav'
import MobileNav from './MobileNav'

const Container = styled.div`
  position: fixed;
  display: flex;
  flex-direction: row;
  z-index: 99;
  justify-content: center;
  width: 100%;
`

const Header = () => {
  const [isDesktop, setDesktop] = useState()
  const updateMedia = () => {
    setDesktop(window.innerWidth > 768)
  }
  function sendMail () {
    const link = "mailto:tylerflores60@yahoo.com?subject=I'm interested in visual work!"
    window.location.href = link
  }

  useEffect(() => {
    setDesktop(window.innerWidth > 768)
  })

  useEffect(() => {
    window.addEventListener('resize', updateMedia)
    return () => window.removeEventListener('resize', updateMedia)
  }, [updateMedia])

  return (
    <Container>
      {isDesktop ? <DeskNav /> : <MobileNav />}
    </Container>
  )
}

export default Header
