import styled from 'styled-components'
import { motion } from 'framer-motion'

const SectionNum = styled(motion.p)`
  font-size: 65px;
  position: absolute;
  font-weight: 700;
  color: ${props => props.theme.colors.secondary};
  opacity: 0.5;
  filter: opacity(40%);
`

export default SectionNum
