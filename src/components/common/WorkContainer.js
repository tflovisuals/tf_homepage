import { theme } from 'src/theme'
import styled from 'styled-components'

const WorkContainer = styled.div`
  margin: 5rem auto;
  overflow-y: hidden;
  overflow-x: scroll;
  white-space:nowrap;
  width: 100%;
  -webkit-overflow-scrolling: touch;

  ::-webkit-scrollbar {
    width: 10px;
    height: 10px;
    display: none;
  }

  ::-webkit-scrollbar-thumb {
    background: rgba(90, 90, 90);
  }

  ::-webkit-scrollbar-track {
    background: rgba(0, 0, 0, 0.2);
  }

  @media screen and (${props => props.theme.size.m}){
    /* align-items: center; */
    /* height: auto; */
  }

`

export default WorkContainer
