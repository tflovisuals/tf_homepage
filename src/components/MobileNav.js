import React, { useState } from 'react'
import styled from 'styled-components'
import NavToggle from './NavToggle'
import { motion } from 'framer-motion'
import { animateScroll as Scroll, Link } from 'react-scroll'

const NavLinksContainer = styled.nav`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  z-index: 9999;
  position: fixed;
  width: 100%;
`
const MotionWrapper = (props) => (
  <motion.div
    animate={{ x: 100 }}
    transition={{ type: 'spring', stiffness: 100 }}
    {...props}
  />
)

const LinksWrapper = styled.ul`
  margin: 0;
  padding: 0;
  display: flex;
  height: 100%;
  list-style: none;
  background-color: #fff;
  width: 100%;
  flex-direction: column;
  position: fixed;
  top: 0px;
  left: 0;
  background-color: ${(props) => props.theme.colors.primary};
`

const LinkItem = styled.li`
  width: 100%;
  padding: 0 1.1em;
  color: ${(props) => props.theme.colors.textinvert};
  font-weight: 500;
  font-size: 20px;
  display: flex;
  margin-bottom: 2em;
  margin-top: 2em;
  margin-left: 1em;
  letter-spacing: 0.1em;

  :hover,
  :focus
  {
    color: ${(props) => props.theme.colors.secondary};
  }
`

const Promo = styled.a`
  color: ${(props) => props.theme.colors.primary};
  word-spacing: 0.25em;
  font-size: 20px;
  background-color: ${(props) => props.theme.colors.secondary};
  width: 55%;
  height:40px;
  line-height: 30px;
  padding: 4px;
  margin-left: 1em;

  :hover,
  :focus
  {
    color: ${(props) => props.theme.colors.textinvert};
    cursor: pointer;
    background-color: ${(props) => props.theme.colors.primary};
    border: 1px solid ${(props) => props.theme.colors.secondary};
  }
`

const Marginer = styled.div`
  height: 2em;
`

const MobileNav = props => {
  const [isOpen, setOpen] = useState(false)
  function scrolling () {
    Scroll.scrollTo('two')
  }
  return (
    <NavLinksContainer>
      <NavToggle isOpen={isOpen} toggle={() => setOpen(!isOpen)} />
      {isOpen && (
        <LinksWrapper>
          <motion.div
            style={{ width: '80%', height: '80%' }}
            transition={{ type: 'spring', duration: 1, bounce: 0.45, ease: 'anticipate' }}
            animate={{
              x: 40,
              y: 70,
              backgroundColor: '#050801',
              boxShadow: '10px 10px 0 rgba(0, 0, 0, 0.2)',
              position: 'fixed',
              opacity: 1,
              scale: 1.0,
              transitionEnd: {
                display: ''
              }
            }}
          >
            <LinkItem onClick={scrolling}>
              <Link to='one' smooth spy duration={1000} onClick={() => { if (isOpen) { setOpen(false) } }}>
                Home
              </Link>
            </LinkItem>
            <LinkItem>
              <Link to='two' smooth duration={1000} offset={-70} onClick={() => { if (isOpen) { setOpen(false) } }}>About</Link>
            </LinkItem>
            <LinkItem>
              <Link to='three' smooth duration={1000} offset={-70} onClick={() => { if (isOpen) { setOpen(false) } }}>Work</Link>
            </LinkItem>
            <LinkItem>
              <Link to='four' smooth duration={1000} offset={-70} onClick={() => { if (isOpen) { setOpen(false) } }}>Testimonials</Link>
            </LinkItem>
            <Promo>
              <Link to='five' smooth duration={1000} offset={-70} onClick={() => { if (isOpen) { setOpen(false) } }}>Contact</Link>
            </Promo>
          </motion.div>
        </LinksWrapper>
      )}
    </NavLinksContainer>
  )
}

export default MobileNav
