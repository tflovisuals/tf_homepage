import React from 'react'
import styled from 'styled-components'

const WorkItem = ({ post }) => {
  const { id, caption, media_type, media_url } = post

  let postType

  switch (media_type) {
    case 'CAROUSEL_ALBUM':
      postType = (
        <MediaLink href={`${media_url}`}>
          <WorkImg
            // width='400'
            // height='400'
            id={id}
            src={media_url}
            alt={caption}
          />
        </MediaLink>
      )
      break

    case 'VIDEO':
      postType = (
        <MediaLink href={`${media_url}`}>
          <WorkVid
            // width='100%'
            // height='100%'
            src={media_url}
            type='video/mp4'
            autoPlay
            muted
            loop
            playsinline
          />
        </MediaLink>
      )
      break

    default:
      postType = (
        <MediaLink href={`${media_url}`}>
          <WorkImg
            // width='100%'
            // height='auto'
            id={id}
            src={media_url}
            alt={caption}
          />
        </MediaLink>
      )
      break
  }
  return (
    <Container>
      {postType}
      
    </Container>
  )
}

const Container = styled.div`
  width: 400px;
  height: 400px;
  margin: 1rem;
  padding: 1rem;
  display: inline-block;

  @media screen and (${props => props.theme.size.m}){
    width: 300px;
    height: 300px;
  }
`

const WorkImg = styled.img`
  width: 100%;
  height: 100%;
`

const WorkVid = styled.video`
  width: 100%;
  height: 100%;
`

const MediaLink = styled.a`
  /* hover:  */
`

export default WorkItem
