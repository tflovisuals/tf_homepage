import styled from 'styled-components'
import OptionItem from './OptionItem'


const Selector = ({ name, placeholder, items, val, handleChange}) => {
  return (
    <>
      <SelectorStyled value={val} multiple name={name} onChange={handleChange}>
        <Option value={''} disabled hidden>{placeholder}</Option>
        {items.map((item, i) => (
          <OptionItem value={item} key={i} />
        ))}
      </SelectorStyled>
    </>
  )
}

const SelectorStyled = styled.select`
  color: ${props => props.theme.colors.white};
  background: transparent;
  margin: 1rem 0.5rem;
  font-size: 16px;
  border:none;
  width: 45%;
  height: auto;
  padding: 0.5rem;
  -moz-appearance: menulist;
  -webkit-appearance: menulist;
  appearance: menulist;

  :focus, :enabled, :scope {
    outline: none;
    background: none;
  }
`

const Option = styled.option`
  appearance: none;
`

export default Selector
