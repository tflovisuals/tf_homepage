import React, { useState } from 'react'
import styled from 'styled-components'

const OptionItem = ({ value, label }) => {
  const [isChecked, setIsChecked] = useState(false)
  return (
    <OptionStyled value={value}>
      {value}
    </OptionStyled>
  )
}

const OptionStyled = styled.option`
  cursor: pointer;

  :focus {
    outline: none;
    background-color: red;
  }

  :checked{
    background: ${props => props.theme.colors.secondary};
  }

`

export default OptionItem
