import React from 'react'
import styled from 'styled-components'

const Field = ({ name, val, placeholder, onChange }) => {
  return (
    <>
      <FieldStyled name={name} value={val} placeholder={placeholder} onChange={(e) => { onChange(e) }} />
    </>
  )
}

const FieldStyled = styled.input`
  border: 0px;
  border-bottom: 2px solid ${props => props.theme.colors.white};
  width: 45%;
  height: 4rem;
  background-color: transparent;
  color: ${props => props.theme.colors.white};
  margin: 1rem 0.5rem;
  font-size: 16px;

  :focus {
      outline: none;
      border-color: ${props => props.theme.colors.secondary};
  }


  @media screen and (${props => props.theme.size.m}) {
    width: 80%;
  }
`

export default Field
