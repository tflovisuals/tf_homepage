import styled from 'styled-components'

export const Container = styled.div`
  background-color: ${props => props.theme.colors.primary};
  display: flex;
  flex-direction: column;
  margin: 0 10%;
`

export const TitleWrapper = styled.div`
  direction: ltr;
`

export const NumWrapper = styled.div`
direction: ltr;
position: absolute;

  @media screen and (${props => props.theme.size.m}) {
    position: absolute;
  }
`

export const InquireWrapper = styled.div`
  display: flex;
  flex-direction: row;
  direction: ltr;

  @media screen and (${props => props.theme.size.m}) {
    flex-direction: column;
  }
`

export const FieldWrapper = styled.form`
  display: flex;
  flex-direction: column;
  direction: ltr;
  align-items: center;
  vertical-align: middle;
  width: 100%;
`

export const SocialWrapper = styled.div`
`
