import React, { useEffect, useState } from 'react'
import axios from 'axios'
import * as s from './ContactStyle'

import Title from '../common/Title'
import SectionNum from '../common/SectionNum'
import Field from 'src/components/Field'
import ReactSelector from '../ReactSelector';
import TextArea from '../TextArea'
import SubmitContactBtn from '../SubmitContactBtn'
import { elementsSelect, initialState } from 'src/data/ContactElements'

const Contact = () => {

  const [{
    name, email, phone, location, eventDate, needs, categories, message
  }, setField] = useState({
    name: '',
    email: '',
    phone: '',
    location: '',
    eventDate: '',
    needs: [],
    categories: [],
    message: ''
  })

  const HandleChange = (e) => {
    e.preventDefault();
    const {name, value} = e.target
    setField((prevState) => ({ ...prevState, [name]: value }))
  }

  const clearState = () => {
    setField({...initialState})
  }

  const HandleSelectChange = (e) => {
    let name = ''
    let value = ''
    e.map(itm => {
      name = itm.name
      value = itm.value
    })
    if(name === 'needs' || name === 'categories'){
      if(needs.includes(value) || categories.includes(value)){
        setField((prevState) => ({...prevState, [name]: prevState[name].filter((e) => (e !== value))}))
      }
      else setField((prevState) => ({...prevState, [name]: [...prevState[name], value]}))
    }
  }

  const HandleSubmit = async () => {
    const config = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        accept: 'application/json'
      }
    }

    const body = {name, email, phone, location, eventDate, needs, categories, message}
    await axios.post('/api/inquire', body, config).then(
      function(response) {
        alert('Thanks for inquiring, I will contact you ASAP!')
        setTimeout(() => {
          clearState();
        }, 1000);
      }
    ).catch(
      function(err) {
        const errors = err.response.data.error.errors
        for (var message in errors) {
          // Validation toast.
          alert(errors[message].message)
        }
      }
    )
  }

  return (
    <s.Container id='five'>
      <s.TitleWrapper>
        <Title>Contact</Title>
      </s.TitleWrapper>
      <s.InquireWrapper>
        <s.NumWrapper>
          <SectionNum>05</SectionNum>
        </s.NumWrapper>
        <s.FieldWrapper>
          <Field name='name' placeholder='Name' val={name} onChange={HandleChange} />
          <Field name='email' placeholder='Email' val={email} onChange={HandleChange} />
          <Field name='phone' placeholder='Phone' val={phone} onChange={HandleChange} />
          <Field name='location' placeholder='Location of Event' val={location} onChange={HandleChange} />
          <Field name='eventDate' placeholder='Date of Event: 01-13-2022' val={eventDate} onChange={HandleChange} />
          <TextArea name='message' type='text' placeholder='Message' rows={10} val={message} onChange={HandleChange} />
          <ReactSelector cName={'needs'} pHolder={'Needs'} opts={elementsSelect.needs} onChange={HandleSelectChange} name={'needs'}/>
          <ReactSelector cName={"categories"} pHolder={'Categories'} opts={elementsSelect.category} isMulti name={'categories'} onChange={HandleSelectChange}/>
          <SubmitContactBtn name='submit' submit={HandleSubmit} content='Submit' />
        </s.FieldWrapper>
        <s.SocialWrapper />
      </s.InquireWrapper>
    </s.Container>
  )
}

export default Contact
