import styled from 'styled-components'

export const Container = styled.div`
  background-color: ${props => props.theme.colors.primary};
  display: flex;
  flex-direction: column;
  margin: 1rem 10%;

  @media screen and (${props => props.theme.size.m}){
    margin: 10rem 10%;
  }
`
