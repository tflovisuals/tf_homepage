import React from 'react'
import WorkContainer from '../common/WorkContainer'
import Title from '../common/Title'
import SectionNum from '../common/SectionNum'
import WorkItem from '../WorkItem'

// Import styles
import * as s from './WorkStyles'

const Work = ({ igPosts }) => {
  return (
    <s.Container id='three'>
      <Title>Work</Title>
      <SectionNum>03</SectionNum>
      <WorkContainer>
        {igPosts.map((post, i) => (
          <WorkItem key={i} post={post} />
        )
        )}
      </WorkContainer>
    </s.Container>
  )
}


export default Work
