import React from 'react';
import Select from 'react-select'
import makeAnimated from 'react-select/animated';
import styled from 'styled-components'

const animatedComp = makeAnimated()

const ReactSelector = ({cName, pHolder, opts, name, onChange}) => {
  return (
    <>
        <StyledDiv>
            <StyledSelector
                className={cName} placeholder={pHolder} options={opts} isMulti name={name}
                onChange={onChange}
                isClearable
                components={animatedComp}
                closeMenuOnSelect={false}
                theme={(theme) => ({
                    ...theme,
                    borderRadius: 0,
                    colors: {
                      ...theme.colors,
                    },
                    menu: {
                        background: 'black'
                    }
                  })}
            />
        </StyledDiv>
    </>
  )
};

const StyledDiv = styled.div`
    margin: 1rem 0.5rem;
    width: 45%;

    @media screen and (${props => props.theme.size.m}) {
    width: 80%;
  }

`

const StyledSelector = styled(Select)`
    border: none;
`

export default ReactSelector