import React, { useEffect, useState } from 'react'
import styled, { css } from 'styled-components'
import { Link, animateScroll as scroll } from 'react-scroll'
import { theme } from 'src/theme'

const Nav = styled.nav`
  display: flex;
  flex-direction: row;
  width: auto;
  margin-left: 0px auto;
`
const Navbar = styled.ul`
  list-style-type: none;
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: row;
  transition-timing-function: ease-in;
  transition: 1s;
  margin-right: 0em;

  @media (max-width: 768px) {
    flex-flow: column nowrap;
    position: fixed;
    transform: ${(props) => props.open ? 'translateX(50%)' : 'translateX(100%)'};
    top: -16px;
    right: 0;
    height: 100%;
    width: 180px;
    padding-top: 3.5rem;
    transition: transform 0.3s ease-in-out;
    z-index: 9;
    justify-content: normal;

    li {
      color: #000;
      margin-right: 34px;
    }
      &:hover {
        color: #0DADEA;
      }
    }
`
const NavItem = styled.li`
  margin: 12px;
  color: #FFFFFF;
  font-size: 20px;
  padding: 1em;

  :hover,
  :focus,
  :active
  {
    color: ${(props) => props.theme.colors.secondary};
    cursor: pointer;
  }

  &.top{
    padding: 2.5em 0em;
    color: transparent;
  };
`

const DeskNav = () => {
  const [scrolling, setScrolling] = useState(true)

  const navbarControl = () => {
    if (window.scrollY > 150) {
      setScrolling(false)
    } else {
      setScrolling(true)
    }
  }
  useEffect(() => {
    window.addEventListener('scroll', navbarControl)
    return () => window.removeEventListener('scroll', navbarControl)
  }, [])

  return (
    <Nav>
      <Navbar>
        <NavItem><Link activeClass='active' to='one' smooth spy duration={1000}>Home</Link></NavItem>
        <NavItem><Link activeClass='active' to='two' smooth spy offset={-50} duration={1000}>About</Link></NavItem>
        <NavItem><Link activeClass='active' to='three' smooth spy offset={-50} duration={1000}>Work</Link></NavItem>
        <NavItem><Link activeClass='active' to='four' smooth spy offset={-50} duration={1000}>Testimonial</Link></NavItem>
        <NavItem><Link activeClass='active' to='five' smooth spy offset={-50} duration={1000}>Contact</Link></NavItem>
      </Navbar>
    </Nav>
  )
}

export default DeskNav
