import React from 'react'
import styled from 'styled-components'

const TestimonialCard = ({ customerReview, ...props }) => {
  const {name, event, date, review } = customerReview
  return (
    <CardWrapper>
      <ContentWrapper>
        <CardTitle>{name}</CardTitle>
        <EventDate>{`${event}`}</EventDate>
        <Review>{review}</Review>
      </ContentWrapper>
    </CardWrapper>
  )
}

const CardWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin: 3rem auto;
`
const ContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const CardTitle = styled.h1`
  color: ${props => props.theme.colors.white};
  line-height: 0rem;
`

const EventDate = styled.h2`
  color: ${props => props.theme.colors.white};
  font-weight: 500;
  line-height: 0rem;
`

const Review = styled.p`
  font-size: 14px;
  color: ${props => props.theme.colors.white};
  line-height: 20px;
  text-align: justify;
`

export default TestimonialCard
