import React, { Fragment } from 'react'
import styled from 'styled-components'

// Import components
import Landing from 'src/components/landing/landing'
import About from 'src/components/about/About'
import Work from 'src/components/work/Work'
import Testimonial from 'src/components/testimonial/Testimonial'
import Contact from 'src/components/contact/Contact'

const Main = styled.main`
  display: flow-root;
`

const HomepageContainer = ({ instagramPosts, reviews }) => {
  return (
    <>
      <Main>
        <Landing />
        <About />
        <Work igPosts={instagramPosts} />
        <Testimonial reviews={reviews} />
        <Contact />
      </Main>
    </>
  )
}

export default HomepageContainer
