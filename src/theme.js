export const theme = {
  colors: {
    // primary: '#050801',
    primary: '#050801',
    // secondary: '#5E2986',
    secondary: '#6F6F71',
    third: '#333333',
    white: '#FFFFFF',
    textprime: '#3E3E3E',
    textsecond: '#292724',
    textinvert: '#FFFFFF'
  },
  size: {
    xxs: 'max-width: 320px',
    xs: 'max-width: 375px',
    sm: 'max-width: 425px',
    m: 'max-width: 768px',
    lg: 'max-width: 1024px',
    xl: 'max-width: 1440px',
    xxl: 'max-width: 2560px'
  },
  alertType: {
    success: '#FFE600',
    danger: '#333333',
    info: '5BC0DE',
    warning: 'F0AD4E'
  },
  alertTxtColor: {
    success: '#3E3E3E',
    danger: '#FFF'
  }
}
