export const elements = {
  needs: ['Visual Films', 'Photos'],
  category: ['Wedding', 'Outdoors', 'Hunting', 'Fishing', 'Auto', 'Other']
}


export const elementsSelect = {
  needs: [{value:'Visual Films' , label:'Visual Films', name: 'needs' }, {value: 'Photos', label:'Photos', name: 'needs'}],
  category: [{value: 'Wedding', label: 'Wedding', name: 'categories'},
    {value: 'Outdoors', label: 'Outdoors', name: 'categories'},
    {value: 'Hunting', label: 'Hunting', name: 'categories'},
    {value: 'Fishing', label: 'Fishing', name: 'categories'},
    {value: 'Auto', label: 'Auto', name: 'categories'},
    {value: 'Other', label: 'Other', name: 'categories'}
  ]
}

export const initialState = {
    name: '',
    email: '',
    phone: '',
    location: '',
    eventDate: '',
    needs: [],
    categories: [],
    message: ''
}