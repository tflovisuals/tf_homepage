import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { normalize } from 'styled-normalize'
import App from 'next/app'

import Layout from 'src/components/Layout'

import { theme } from 'src/theme'

const GlobalStyle = createGlobalStyle`
  ${normalize}
  // GLOBAL STYLE a
  /* html { background-color: ${props => props.theme.colors.primary};} */
  body, html {
    padding: 0;
    margin: 0;
    height: 100%;
    background-color: ${props => props.theme.colors.primary};
    font-family: proxima-nova, Segoe UI, Roboto, Oxygen,
      Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }
`
class MyApp extends App {
  render () {
    const { Component, pageProps, router } = this.props
    return (
      <div>
        <ThemeProvider theme={theme}>
          <GlobalStyle />
          <Layout>
            <Component router={router} {...pageProps} />
          </Layout>
        </ThemeProvider>
      </div>
    )
  }
}

export default MyApp
