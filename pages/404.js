import React from 'react'

const ErrorPage = () => {
  return (
    <div>
      <p>
        Ooops... something went wrong here. Lets take you back.
      </p>
    </div>
  )
}

export default ErrorPage
