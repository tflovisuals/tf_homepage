import HomepageContainer from '../src/containers/HomepageContainer'
import axios from 'axios'

// export default HomepageContainer

function Index ({ instagramPosts, reviews }) {
  return (
    <HomepageContainer instagramPosts={instagramPosts} reviews={reviews} />
  )
}

export async function getStaticProps (context) {
  let Posts = []
  let Reviews = []

  const igtoken = process.env.IG_TOKEN;

  try {
    const config = {
      method: 'POST',
      headers: {
        'content-type': 'application/json',
        accept: 'application/json'
      }
    }
    const IGRefreshedToken = await axios.get(`https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=${igtoken}`).then((res) => {
      return res.data;
    });

    const{ access_token, token_type, expires_in } = IGRefreshedToken;
    await axios
      .get(`https://graph.instagram.com/me/media?fields=id,media_type,media_url,caption&access_token=${access_token}`)
      .then((resp) => {
        Posts = resp.data.data
      })

    await axios
      .get('https://www.tflovisuals.com/api/reviews', config)
      .then((resp) => {
        Reviews = resp.data.data
      })
  } catch (err) {
    console.log(err)
  }

  return {
    props: {
      instagramPosts: Posts,
      reviews: Reviews
    }
  }
}

export default Index
