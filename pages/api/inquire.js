import dbConnect from 'lib/dbconnection'
import Inquire from 'models/Inquire'
import { google } from 'googleapis'
import dotenv from 'dotenv'
import nodemailer from 'nodemailer'
dotenv.config()

export default async function handler (req, res) {
    const { method } = req

    await dbConnect()

    switch (method) {
        case 'POST':
            try{
                // Refresh token on Email request
                const refreshToken = process.env.OAUTH_REFRESH_TOKEN
                const oAuth2Client = new google.auth.OAuth2(process.env.OAUTH_CLIENT_ID, process.env.OAUTH_CLIENT_SECRET, process.env.REDIRECT_URI)
                oAuth2Client.setCredentials({ refresh_token: refreshToken })
                // Get accessToken
                const accessToken = await oAuth2Client.getAccessToken()
                // send mail with defined transport object

                const transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                    type: 'OAuth2',
                    user: process.env.EMAIL_USER,
                    clientId: process.env.OAUTH_CLIENT_ID,
                    clientSecret: process.env.OAUTH_CLIENT_SECRET,
                    refreshToken: refreshToken,
                    accessToken: accessToken
                    },
                    secure: true
                })

                // Verify transporter
                transporter.verify((err, success) => {
                    err
                    ? console.log(err)
                    : console.log(`=== Server is ready to take messages: ${success} ===`)
                })

                const inquire = await Inquire.create(req.body)
                const { name, email, phone, location, eventDate, message, categories, needs } = req.body
                // create reusable transporter object using the default SMTP transport
                transporter.sendMail({
                    from: 'info@doalllabs.io', // sender address
                    to: 'tyler.flores@tflovisuals.com', // list of receivers
                    subject: 'New Visual Submission - T-Flo Visuals', // Subject line
                    text: `Prospect info: \n name: ${name} \n email: ${email} \n phone: ${phone} \n location: ${location} \n eventDate: ${eventDate} \n message: ${message} \n needs: ${needs} \n categories: ${categories}`
                }, function (err, info) {
                    if (err)console.log(err)
                    else res.status(200).json({ msg: 'Thanks for submitting!', success: true, data: inquire })
                })
               res.status(200).json({ msg: 'Thanks for submitting!', success: true, data: inquire })
            }
            catch(err){
                res.status(400).json({success: false, error: err})
            }
            break
        default:
            res.status(400).json({ success: false})
            break
    }
}