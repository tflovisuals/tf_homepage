import mongoose from 'mongoose'

const MONGODB_URI = process.env.MONGODB_URI

if (!MONGODB_URI) {
  throw new Error(
    'Please define the MONGODB_URI environment variable inside .env.local'
  )
}

let cached = global.mongoose

if (!cached) {
  cached = global.mongoose = { conn: null, promise: null }
}

async function connectDB () {
  // if (mongoose.connections[0].readyState) {
  //   // Use current db connection
  //   return handler(req, res);
  // }
  // // Use new db connection
  // await mongoose.connect(process.env.MONGODB_URI, {
  //   useUnifiedTopology: true,
  //   useNewUrlParser: true
  // });
  // return handler(req, res);
  if (cached.conn) {
    return cached.conn
  }

  if (!cached.promise) {
    const opts = {
      useNewUrlParser: false,
      useUnifiedTopology: false,
      bufferCommands: false,
      bufferMaxEntries: 0,
      useFindAndModify: false,
      useCreateIndex: false

    }

    cached.promise = mongoose.connect(MONGODB_URI).then(mongoose => {
      return mongoose
    })
  }
  cached.conn = await cached.promise
  return cached.conn
};

export default connectDB
